import uuid

from django.contrib.auth.models import User
from django.db import models

from cross_database_field.fields import *


# region Local database
class ExtraData(models.Model):
    stuff = models.CharField(
        max_length=50,
        default="stuff",
        null=True
    )

    def __str__(self):
        if self.stuff is None:
            return "Empty"
        return self.stuff


class UUIDTestCustomModel(CrossDatabaseCapableModel):
    user_proxy = UUIDCrossDatabaseField(
        to="testing.models.UserProxy",
        remote_db="users"
    )
    extra_relation = models.ForeignKey(
        ExtraData,
        on_delete=models.SET_NULL,
        default=None,
        null=True
    )
    extra_data = models.CharField(
        max_length=50,
        default=None,
        null=True
    )


class NumericTestCustomModel(CrossDatabaseCapableModel):
    user_proxy = CrossDatabaseField(
        to="django.contrib.auth.models.User",
        remote_db="users",
        null=True,
        default=None
    )
    extra_relation = models.ForeignKey(
        ExtraData,
        on_delete=models.SET_NULL,
        default=None,
        null=True
    )
    extra_data = models.CharField(
        max_length=50,
        default=None,
        null=True
    )

# endregion


# region Users database

# UUID pk test case
class UserProxy(models.Model):
    id = models.UUIDField(
        primary_key=True,
        default=uuid.uuid4,
        editable=False
    )
    user = models.OneToOneField(
        User,
        on_delete=models.CASCADE
    )

    def __str__(self) -> str:
        if self.user is None:
            return "Empty"
        return str(self.user)

    class Meta:
        app_label = "auth"


# Numeric pk test case
class PseudoProxy(models.Model):
    username = models.CharField(max_length=200)

    def __str__(self) -> str:
        if self.username is None:
            return "Empty"
        return str(self.username)

    class Meta:
        app_label = "auth"

# endregion
