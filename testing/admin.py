from django.contrib import admin

from .models import *


admin.site.register(UserProxy)
admin.site.register(PseudoProxy)
admin.site.register(ExtraData)
admin.site.register(UUIDTestCustomModel)
admin.site.register(NumericTestCustomModel)\
