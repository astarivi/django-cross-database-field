from django.apps import AppConfig


class CrossDatabaseFieldConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'cross_database_field'
